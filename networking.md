# Configuración de red

Por defecto, cuando creamos una máquina virtual basada en una *box* de Vagrant, ésta incorpora una interfaz de red configurada en modo NAT. A través de esta interfaz de red nos conectamos al servicio SSH (posiblemente con una redirección hacia el puerto 22 para permitir el uso simultáneo de varias máquinas en nuestro equipo), y con salida hacia Internet desde la máquina virtual.
Esta configuración de NAT la gestiona VirtualBox, y hace que no podamos acceder a los recursos internos de la máquina virtual, exceptuando la conexión SSH.

En diferentes ocasiones tendremos que añadir nuevas interfaces de red para comunicarnos con la máquina virtual de forma directa, o bien crear redes internas entre diferentes máquinas virtuales. Todas estas configuraciones las podemos gestionar desde el fichero *Vagrantfile*.

> Debemos prestar atención a la interfaz creada por defecto ya que al ser la encargada de gestionar el tráfico SSH, si llegamos a realizar alguna modificación, o bloqueo, nos quedamos sin acceso a la máquina virtual.

## Interfaz de red en modo *bridge*

Cuando sea necesario tener conectividad con nuestra máquina anfitrión desde el interior de la máquina virtual, y viceversa, tendremos que añadir una interfaz de red en modo *puente*. Lo podemos hacer añadiendo la siguiente línea al fichero *Vagrantfile*:

```
config.vm.define "nodo01" do |nodo|
[...]
  nodo.vm.network "private_network", ip: "192.168.33.10"
[...]
```

De este modo, en nuestro equipo anfitrión se creará una nueva interfaz de modo automático, que en este ejemplo, tendrá asignada la IP 192.168.33.1, a través de la cual nos podremos comunicar con la IP de la máquina virtual, 192.168.33.10.

## Interfaz de red con acceso interno

De forma similar al caso anterior, podemos añadir una interfaz adicional, pero en este caso sin acceso al exterior. VirtualBox crea un concentrador virtual al que conecta estas interfaces, que será útil para realizar conexiones internas entre diferentes máquinas virtuales, sobre todo en configuraciones de clusters.

```
config.vm.define "nodo01" do |nodo|
[...]
  nodo.vm.network "private_network", ip: "192.168.1.10", virtualbox__intnet: "interna"
[...]
```

Como se puede apreciar en el ejemplo anterior, se añade la opción *virtualbox__intnet*, junto con el nombre personalizado (*interna) para identificar las posibles redes internas (podemos tener tantas como deseemos).

> Atención al doble carácter `__` que hay entre *virtualbox* e *intnet*

## Más información

[Vagrant networking documentation](https://www.vagrantup.com/docs/networking/)
