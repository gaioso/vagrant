# Ficheros y directorios

La siguiente imagen muestra un resumen de los ficheros y directorios más importantes para ``$ vagrant up``, y que nos ayudará a entender mejor el proceso de arranque de un sistema invitado.

![image](/images/Fig03-02.jpg)

El directorio etiquetado como A es donde se guarda el código fuente del proyecto (puede ser creado con el comando ``$ git clone``). Es el lugar donde podemos ejecutar todos los comandos, tales como ``$ vagrant up`` o ``$ vagrant status``. Aquí podemos encontrar el fichero Vagrantfile y el directorio .vagrant.

Durante la etapa 1, Vagrant descarga el fichero 'box' desde el servidor remoto y lo instala en el sistema dentro del directorio para las imágenes, que está etiquetado como B. Esta imagen puede ser reutilizada en varios proyectos. Puede resultar útil, que este directorio pueda ser accesible al resto de usuarios del sistema para aprovechar las imágenes descargadas.

En la etapa 2, la imagen guardada en el directorio B se copia y descomprime en el direcotrio C, que sólo será utilizado por el sistema invitado.

Y durante la etapa 3, de momento nos fijaremos únicamente en la creación del directorio .vagrant.

## Estados de los sistemas invitados

Existen cinco estados en los que se puede encontrar un sistema invitado, y que se visualizan a través del comando `$ vagrant status`:

* running
* poweroff
* saved
* not created
* aborted

