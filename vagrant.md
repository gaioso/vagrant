# Vagrant

Vagrant es un software disponible bajo una licencia MIT, que fue desarrollado originalmente por Mitchell Hashimoto. El código fuente está disponible en [Github](https://github.com/mitchellh/vagrant>).

Siguiendo la terminología de Vagrant, la solución de virtualización que está en la capa inferior se denomina «proveedor» (provider). Para poder usar Vagrant debemos instalar por lo menos un «proveedor». La lista oficial de proveedores es la siguiente:

* VirtualBox
* VMWare
* Docker
* Hyper-V

Aunque es posible trabajar con los entornos de virtualización anteriores sin usar Vagrant, se requiere de un gran esfuerzo para realizar las tareas de forma manual. Vagrant automatiza muchas de ellas a través del comando $ vagrant up. En la mayoría de casos, los desarrolladores sólo deben usar este comando para arrancar la aplicación como si se tratara de una caja negra, de la que desconocen su interior.

## Instalación de software

Para poder realizar las siguientes prácticas, es necesario disponer de los siguiente paquetes:

* Git
* VirtualBox
* Vagrant

## Consultar versión

Podemos comprobar la disponibilidad de Vagrant ejecutando el siguiente comando:

	$ vagrant --version
	Vagrant 1.9.1

## Configuración básica

Por defecto, Vagrant almacena las VMs 'boxed' en la ruta `~/.vagrant.d`. Estas 'boxes' suelen ser ficheros de gran tamaño, y podemos considerarlas como una 'imagen modelo' de las MVs que tendremos en ejecución. Se pueden compartir entre diferentes usuarios, con el objetivo de optimizar el espacio utilizado en disco.  

La ubicación del directorio principal de Vagrant puede modificarse a través de la variable de entorno **VAGRANT_HOME**. Por ejemplo, a través del siguiente comando:

	$ export VAGRANT_HOME=/ruta/al/directorio

## Ayuda y documentación

Tarde o temprano, tendremos que hacer uso de las fuentes de información para buscar ayuda en el uso de Vagrant. La primera opción puede ser la ayuda interna del propio comando:

	$ vagrant 
	$ vagrant -h

El uso de Vagrant se realizar a través de 'subcomandos', como por ejemplo:

	$ vagrant box 
	$ vagrant package

Durante la ejecución de las prácticas, haremos uso de comandos similares a estos:

	$ vagrant box add 
	$ vagrant box list

Los subcomandos también poseen su propia ayuda si los ejecutamos del siguiente modo:

	$ vagrant box -h 
	$ vagrant box add -h 

Cuando esta primera ayuda no es suficiente, debemos utilizar la [ayuda en línea de Vagrant](https://www.vagrantup.com/intro/getting-started/index.html).
