# Gestión de múltiples máquinas

Vagrant incorpora la capacidad de gestión de varias máquinas de forma simultánea a través de la definición de las mismas en el fichero Vagrantfile. Esta capacidad facilita la gestión de las mismas, ya que podemos ejecutar acciones sobre todas ellas al mismo tiempo, como por ejemplo reiniciarlas o aplicar un *script* de aprovisionamiento.

Veamos un ejemplo:

```
Vagrant.configure(2) do |config|

  config.vm.define "nodo01" do |nodo|
    nodo.vm.box = "centos/7"
    [...]

  config.vm.define "nodo02" do |nodo|
    nodo.vm.box = "centos/7"
    [...]

  [...]
end
```

En este entorno, los comandos `vagrant up` o `vagrant reload` se aplicarán a todas las máquinas definidas en el fichero *Vagrantfile*. Si queremos conectarnos a una de ellas debemos indicarlo en el comando, por ejemplo `vagrant ssh nodo01`.

Si queremos que las máquinas tengan conectividad entre ellas a través de una red privada, debemos aplicar la configuración que se detalla en el capítulo de [Configuración de red](networking.md).

También puede ser necesario aplicar configuraciones comunes a todas las máquinas a través de un aprovisionamiento común, por ejemplo para actualizar el contenido del fichero `/etc/hosts`:

```
$script = <<SCRIPT
echo "192.168.1.10 node01.lpic.lan node01" > /etc/hosts
echo "192.168.1.20 node02.lpic.lan node02" >> /etc/hosts
SCRIPT


Vagrant.configure(2) do |config|

 config.vm.provision "shell", inline: $script

[...]
```

Con la configuración anterior, se aplicará el *script* a cada una de las máquinas definidas más adelante en el fichero `Vagrantfile`.

## Más información

[Vagrant multi-machine documentation](https://www.vagrantup.com/docs/multi-machine/)
