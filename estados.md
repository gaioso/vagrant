# Los estados de una Máquina Virtual

Comenzaremos por analizar las acciones involucradas en la ejecución de $ vagrant up, y las agruparemos en tres etapas. Prestaremos atención a varios ficheros y directorios que están involucrados en el arranque así como el momento en el que se inicia cada una de las etapas. Luego, analizaremos los cinco estados en los que se puede encontrar un sistema operativo invitado.

En el sitio web [Vagrant Cloud](https://app.vagrantup.com/boxes/search) se pueden obtener los datos de las diferentes boxes disponibles, entre las que se encuentran la mayoría de versiones de las principales distribuciones de GNU/Linux.

Para poder crear nuestra primera máquina virtual, partimos de una BoxBase de [Debian Buster](https://app.vagrantup.com/debian/boxes/buster64).

**Antes de empezar**

Comprobamos que no se están ejecutando otras MVs en nuestro equipo:

	$ vagrant global-status

**Nuevo directorio y fichero Vagrantfile**

Debemos crear un nuevo directorio donde guardar el fichero de configuración para nuestra máquina virtual:

	$ mkdir buster

A continuación, creamos el fichero `Vagrantfile` con el siguiente comando:

	$ vagrant init debian/buster64
	A `Vagrantfile` has been placed in this directory. You are now
	ready to `vagrant up` your first virtual environment! Please read
	the comments in the Vagrantfile as well as documentation on
	`vagrantup.com` for more information on using Vagrant.

> Si queremos generar una versión reducida del fichero de configuración, usaremos la opción `-m`.

> `$ vagrant init -m debian/buster64`

## Vagrantfile

La configuración de las MVs usadas en nuestros ejercicios está guardada en el fichero `Vagrantfile`. Cada proyecto tiene su propio fichero, donde se guardan las propiedades de la MV. Este fichero está escrito en Ruby, y contiene, entre otras cosas, el nombre de la *box* inicial desde la que construír la MV.

Como el fichero *Vagrantfile* almacena los detalles de la configuración de la MV, cualquier administrador que tenga acceso a dicho fichero, podrá acceder al mismo entorno que el resto de compañeros.

El fichero Vagrantfile que se ha generado en los pasos anteriores debe ser similar al siguiente:

	Vagrant.configure("2") do |config|
	  config.vm.box = "debian/buster64"
	end

## Imagen de una Máquina Virtual

La imagen usada por Vagrant para crear y ejecutar el sistema invitado está formada por un único fichero, al que se suele denominar como «box file», o fichero imagen. La extensión usada para este tipo de fichero es `.box`, pero no es obligatorio.

## Iniciando una Máquina Virtual

Cuando se ejecuta el comando $ vagrant up la primera vez, veremos una salida similar a la siguiente:

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'debian/buster64' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: >= 0
==> default: Loading metadata for box 'debian/buster64'
    default: URL: https://vagrantcloud.com/debian/buster64
==> default: Adding box 'debian/buster64' (v10.0.0) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/debian/boxes/buster64/versions/10.0.0/providers/virtualbox.box
    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
==> default: Successfully added box 'debian/buster64' (v10.0.0) for 'virtualbox'!
==> default: Importing base box 'debian/buster64'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'debian/buster64' version '10.0.0' is up to date...
==> default: Setting the name of the VM: buster_default_1563902101135_31148
==> default: Vagrant has detected a configuration issue which exposes a
==> default: vulnerability with the installed version of VirtualBox. The
==> default: current guest is configured to use an E1000 NIC type for a
==> default: network adapter which is vulnerable in this version of VirtualBox.
==> default: Ensure the guest is trusted to use this configuration or update
==> default: the NIC type using one of the methods below:
==> default: 
==> default:   https://www.vagrantup.com/docs/virtualbox/configuration.html#default-nic-type
==> default:   https://www.vagrantup.com/docs/virtualbox/networking.html#virtualbox-nic-type
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Running 'pre-boot' VM customizations...
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default: 
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default: 
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
==> default: Installing rsync to the VM...
==> default: Rsyncing folder: /media/gaioso/store/vagrant/buster/ => /vagrant

==> default: Machine 'default' has a post `vagrant up` message. This is a message
==> default: from the creator of the Vagrantfile, and not from Vagrant itself:
==> default: 
==> default: Vanilla Debian box. See https://app.vagrantup.com/debian for help and bug reports
```

El proceso de arranque de la máquina está dividido en tres etapas:

* Etapa 1. Descarga e instalación de la 'box' en el sistema anfitrión. ``~/.vagrant.d/boxes``
* Etapa 2. Importación de la 'box' al proyecto. ``~/VirtualBox VMs/``
* Etapa 3. Arranque del sistema.

El nombre del directorio en el cual Vagrant almacena las imágenes descargadas, es el mismo que la URL, pero con el carácter ``/`` sustituído por ``-VAGRANTSLASH-``.

Una vez finalizado el proceso de arranque de la nueva máquina virtual, se devuelve el control al prompt de nuestro sistema. No se aprecia ningún cambio, ya que la máquina se ha iniciado en modo *headless*, y no disponemos de interfaz gráfica (GUI). Para comprobar el estado actual de la máquina recién iniciada, ejecutamos el siguiente comando:

	$ vagrant status

En este directorio existe un subdirectorio oculto con nombre '.vagrant', que es donde se almacena el ID y otra información sobre el sistema invitado. Está preparado para ser creado con el comando `$ vagrant up` y eliminado con `$ vagrant destroy`.

## Acceder a la máquina vía SSH

En este momento, nuestra máquina virtual se encuentra en ejecución y preparada para conectarnos a ella. Tan sólo tenemos que ejecutar el siguiente comando:

	$ vagrant ssh
	vagrant@buster:~$

Por defecto, la sesión se inicia con el usuario *vagrant*, que tiene permisos para ejecutar comandos de administrador (root) a través del comando `sudo` sin que se solicite contraseña. Si fuese necesario introducir la contraseña de este usuario, debemos usar *vagrant*.

## Apagar la máquina

Una vez finalizada la sesión de trabajo, podemos apagar nuestra máquina de forma ordenada con el siguiente comando:

	$ vagrant halt

## Destruír las máquinas virtuales

Cuando hayamos acabado de realizar el trabajo con entorno servidor, podemos destruír las máquinas, o bien, detenerlas:

	$ vagrant halt [ID]
	$ vagrant destroy [ID]

Los [ID] podemos consultarlos en la salida de `$ vagrant global-status`, o bien, situarnos dentro del directorio donde está ubicado en fichero Vagrantfile, que es donde se ejecutó el comando de creación de la MV `$ vagrant up`.
