# Presentación

![image](/images/vagrant.png)

Vagrant es una herramienta que simplifica el trabajo para ejecutar y gestionar máquinas virtuales (MVs). Ofrece las siguientes características:

* Dispone de una interfaz de línea de comandos muy simple para gestionar las MVs. 
* Soporta las principales soluciones de virtualización: [VirtualBox](https://www.virtualbox.org>), [VMWare](https://www.vmware.com) y [Hyper-V](https://en.wikiipedia.org/wiki/Hyper-V).
* Tiene soporte para las principales herramientas de configuración, incluyendo a Ansible, Chef, Puppet y Salt.
* Facilita la distribución y migración de los entornos virtuales.  

Aunque destaca en su uso bajo entornos de desarrollo de aplicaciones web, puede utilizarse para tareas diferentes. Podemos considerarla como una herramienta de propósito general.  Si trabajamos de un modo manual en nuestro entorno de virtualización, tendremos que ejecutar las siguientes operaciones:

* Descargar la imagen de la MV.
* Arrancar la MV.
* Configurar los directorios compartidos y las interfaces de red. 
* Posiblemente, instalar software adicional dentro de la MV.  

Con Vagrant, todas estas tareas (y muchas otras) se pueden automatizar. El comando ``$ vagrant up`` puede ejecutar las siguientes acciones (dependiendo del fichero de configuración):

* Descargar e instalar una MV, si es necesario. 
* Arrancar la MV.
* Configurar varios recursos: RAM, CPU's, conexiones de red y carpetas compartidas. 
* Instalar software adicional en la MV a través de herramientas como Puppet, Chef, Ansible o Salt.

