# Aprovisionamiento

En la sección anterior hemos visto el proceso para configurar una imagen que cumpla con nuestras necesidades, a partir de una 'base box' de origen. Ahora, veremos como automatizar ese proceso de configuración de nuestro entorno. Lo que haremos es guardar los comandos que se deben ejecutar durante el arranque de nuestra máquina en un script. Este proceso automático es lo que se conoce como 'aprovisionamiento'.

Vagrant soporta diferentes herramientas de configuración, como por ejemplo:

* Shell scripts
* Puppet
* Ansible
* Chef

## Proveedores

Las herramientas que se utilizan durante la etapa de aprovisionamiento, se conocen como 'provisioners' (proveedores). Cualquier comando de los que solemos ejecutar en una sesión SSH pueden ser ejecutadas por el proveedor.

## Configuración del aprovisionamiento

La configuración se guarda en el fichero 'Vagrantfile'. El primer parámetro es el nombre del proveedor que se va utilizar. En nuestro ejemplo, usaremos un script de shell:

	Vagrant.configure(2) do |config|
		...
		config.vm.provision "shell", path: "script.sh"
		end

## Múltiples proveedores

Es posible el uso de diferentes proveedores para configurar nuestra máquina. Por ejemplo, podemos usar Shell, Puppet y Ansible en el mismo proyecto:

	Vagrant.configure(2) do |config|
		...
		config.vm.provision "shell", path: "script.sh"
		config.vm.provision "puppet"
		config.vm.provision "ansible", playbook: "playbook.yml"
	end

## ¿Cuando se ejecuta el aprovisionamiento?

Por defecto, sólo se ejecuta durante la primera ejecución de ``$ vagrant up``. Si detenemos el sistema con ``$ vagrant halt`` o ``$ vagrant suspend``, la próxima ejecución de $ vagrant up omitirá la etapa de aprovisionamiento. Aunque podemos modificar ese comportamiento con las opciones ``--provision`` y ``--no-provision``.

	$ vagrant up --provision
	$ vagrant up --no-provision

## Control de versiones de las 'boxes'

Los ficheros de aprovisionamiento que se van utilizar sufrirán diferentes cambios para que se puedan adaptar a la configuración deseada para nuestra imagen. Con la intención de poder controlar las diferentes versiones de dichos ficheros, y poder etiquetarlas, haremos uso de git para la gestión de dichas versiones.

## Jekyll box aprovisionada con Shell Script

En apartados anteriores se usaron diferentes comandos para instalar Jekyll en nuestra 'box' personalizada. Ahora, vamos a automatizar este proceso mediante un script de Shell. La idea es que después de la ejecución del comando ``$ vagrant up``, ya tengamos una imagen con Jekyll instalado, y listo para poder utilizar.

Creamos el directorio de trabajo vacío:

	$ cd /directorio/para/ejercicios
	$ mkdir jekyll-shell

En este nuevo directorio creamos dos ficheros:

* Vagrantfile
* script.sh

Se deja como tarea la escritura del contenido de dichos ficheros.

Cuando se hayan completado ambos ficheros, tan solo tendremos que ejecutar el comando ``$ vagrant up``, para poder disponer de una máquina personalizada con Jekyll.

A continuación, comprobamos en el sistema invitado si podemos usar Jekyll para crear un nuevo blog:

	$ vagrant ssh
	$ jekyll --version

Cuando hayamos realizado las comprobaciones de que la configuración de la máquina es correcta, es momento de guardar una versión de nuestros ficheros de configuración:

	$ git init
	$ echo .vagrant > .gitignore
	$ echo "*.box" >> .gitignore
	$ git add -A
	$ git commit -m "Version inicial"
	$ git tag -a v0.1.0 -m "Release 0.1.0"
	$ git log --oneline --decorate

**Ejercicio**

Crear una nueva 'box' a partir de la imagen creada en el paso anterior.

## Más información

[Vagrant provisioning documentation](https://www.vagrantup.com/docs/provisioning/)
