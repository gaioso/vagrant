# Nuestra primera imagen

En esta sección veremos como crear una imagen a partir de Ubuntu 14.04, sobre la que instalaremos cierto software adicional. Una vez completada la instalación y configuración, la exportaremos a un fichero.

Nos situamos en el escenario en el cual se nos encarga la creación de una imagen para poder crear un blog compartido por el equipo de comunicación. Usaremos Jekyll como herramienta de gestión de los contenidos del blog.

Por lo tanto, el entorno de desarrollo a configurar debe soportar las siguientes características:

* Disponer de las herramientas para procesar los ficheros HTML.
* Poder consultar el resultado del blog a través de la dirección http://localhost:8080.

Se llevarán a cabo diferentes actividades en dos directorios separados:

* ~/box-factory/

* ~/corporate-blog/

En un primer momento, trabajaremos dentro del primer directorio para diseñar la imagen. Este trabajo será ejecutado por los responsables de sistemas. Y, en una segunda etapa, trabajaremos en el segundo directorio, haciendo uso de la imagen creada en el paso anterior, y será gestionado por el equipo de desarrollo.

## Elección de 'base box' y puesta en marcha

Cuando tenemos que crear una nueva imagen, la solución más rápida es partir de una imagen ya existente. Por ejemplo, si queremos preparar un entorno de desarrollo que haga uso de Ubuntu, podemos partir de las imágenes soportadas por Ubuntu:

* ubuntu/trusty64
* ubuntu/trusty32
* ubuntu/precise64
* ubuntu/precise32

En caso de decidirnos por CentOS, Debian, Fedora o FreeBSD, podemos usar las siguientes imágenes:

* centos/7
* debian/stretch64
* fedora/26-cloud-base
* freebsd/FreeBSD-11.1-STABLE

Para nuestro ejercicio haremos uso de Ubuntu 14.04, pero la estrategia es similar para otras distribuciones, y sólo cambiará el modo de instalar software en cada una de ellas.

Empezamos por crear el directorio de trabajo:

	$ cd
	$ mkdir box-factory
	$ cd box-factory

Iniciamos un nuevo proyecto Vagrant:

	$ vagrant init -m ubuntu/trusty32

Levantamos la MV:

	$ vagrant up

## Instalación del software necesario

Con el SO preparado, iniciamos una sesión SSH para instalar el software necesario. A mayores, instalaremos el navegador Lynx para comprobar el estado del sitio web desde la sesión SSH.

	$ vagrant ssh
	# Invitado
	$ sudo apt-add-repository ppa:brightbox/ruby-ng
	$ sudo apt-get update
	$ sudo apt-get install ruby2.1 ruby2.1-dev lynx-cur
	$ sudo gem2.1 install jekyll

Una vez instalado el software podemos comprobar la versión de Jekyll disponible:

	$ jekyll --version

Cerramos la sesión SSH, y ya tenemos nuestra MV para poder empaquetar.

## Creando la imagen

Una vez que disponemos del sistema operativo invitado en ejecución, y que incluye el software necesario, es el momento de crear la imagen:

	# Anfitrión
	$ vagrant package --output box-jekyll.box

## Consultar, instalar y eliminar 'boxes'

A partir de este momento, tomaremos el papel de desarrollador. Tal y como se vió en secciones anteriores, las imágenes deben estar descargadas en nuestro sistema antes de poder utilizarse en Vagrant. La gestión de las imágenes la llevamos a cabo con los siguientes comandos:

	# Anfitrión
	$ vagrant box list
	$ vagrant box add
	$ vagrant box remove

Para añadir una nueva imagen a nuestro directorio .vagrant.d/boxes tenemos que usar el comando $ vagrant box add con dos parámetros, como se muestra a continuación:

	# Anfitrión
	$ vagrant box add [NOMBRE] [DIRECCION]

El **[NOMBRE]** es la etiqueta que deseamos asignarle a la nueva imagen, y la **[DIRECCION]** apunta al fichero que contiene la imagen (con extensión .box).

Por lo tanto, para poder añadir nuestra imagen recién creada:

	$ vagrant box add box-jekyll box-jekyll.box

Y, para eliminar una imagen, usamos el siguiente comando:

	$ vagrant box remove [NOMBRE]

## Usando la imagen 

En este momento, ya estamos en disposición de usar la imagen recién añadida a nuestro sistema. Creamos nuestro nuevo directorio de trabajo, y creamos el nuevo proyecto:

	$ cd
	$ mkdir corporate-blog
	$ cd corporate-blog
	$ vagrant init -m box-jekyll
	$ vagrant up
	$ vagrant ssh

	# Invitado
	$ cd /vagrant
	$ jekyll new -f .
	$ jekyll build
	$ jekyll serve -H 0.0.0.0 --detach
	$ lynx 127.0.0.1:4000

**Ejercicio**:

Configurar la redirección de puertos para que se pueda consultar el blog desde el sistema anfitrión a través del puerto 8080.
