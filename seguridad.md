# Seguridad

## Riesgo #1

El primer riesgo potencial se inicia en la descarga de la imagen desde Internet. El uso de una 'base box' se traduce en el despliegue de un fichero empaquetado con un SO completo y que iniciaremos en nuestro equipo. Es decir, cuando se ejecuta el comando ``$ vagrant up``, iniciamos un SO configurado por otra persona.

Por norma general, debemos usar 'boxes' con una fuente más o menos confiable, por ejemplo, *hashicorp*, *ubuntu* o *chef*.

### Iniciando el SO invitado

Si ya hemos utilizado la 'box' ubuntu/trusty32 en alguno de nuestros proyectos anteriores, tendremos que eliminarla de nuestro sistema con el siguiente comando:

	$ vagrant box remove ubuntu/trusty32

Verificamos que no está presente:

	$ vagrant box list
	$ vagrant box list | grep ubuntu

Ahora, si tenemos presente nuestro fichero 'Vagrantfile', e iniciamos el sistema invitado:

	$ vagrant up

En la salida de este comando podemos observar la URL de descarga:

	defautl: URL: https://atlas.hashicorp.com/ubuntu/trusty32

que la expande en la siguiente dirección:

	default: Downloading: https://atlas.hashicorp.com/ubuntu/boxes/trusty32/versions/14.04/providers/virtualbox.box

Si intentamos descargar la URL anterior con el comando curl:

	$ curl https://atlas.hashicorp.com/ubuntu/boxes/trusty32/versions/14.04/providers/virtualbox.box

obtenemos la siguiente respuesta:

	<html>
		<body>
		You are being <a href="http://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-i386-vagrant-disk1.box">redirected</a>.
		</body>
	</html>

De este modo podemos descargar la imagen que se utilizará en nuestro equipo.

### Configuración por defecto de una MV

La configuración por defecto de una MV tiene las siguientes propiedades:

* Un puerto redirigido: El puerto 2222 del anfitrión está redirigido al puerto 22 del invitado.
* Directorio compartido: El directorio donde está guardado el fichero 'Vagrantfile' es compartido entre el anfitrión y el invitado.

Se puede verificar el estado de las configuraciones anteriores a través de la interfaz de administración de VirtualBox.

> Siempre que hagamos uso de la redirección de puertos de un sistema invitado, debemos asegurarnos de que sólo son accesible por el equipo anfitrión, a no ser que estemos seguros de que queremos aplicar otra configuración diferente.

## Riesgo #2

Como hemos visto, Vagrant sólo publica un puerto del sistema invitado. No importan los servicios que puedan estar ejecutandose en el sistema invitado, el único 'daemon' accesible es sshd. Y además, sólo se permiten conexiones que se inicien en el sistema anfitrión.

Cuando activemos otros puertos, debemos replicar esta configuración. Nuestro sistema invitado no debe exponer ningún servicio a toda la red. Una forma de aplicar esta restricción es hacer uso del parámetro host_ip, dentro de la regla :forwarded_port:

	config.vm.network :forwarded_port, guest: 80, host: 8888, host_ip: "127.0.0.1"

### Directorios compartidos

Cuando ejecutamos ``$ vagrant up``, se comparte el directorio del proyecto, como se puede comprobar en la salida del comando:

	==> default: Mounting shared folders...
	default: /vagrant => /ejemplos/nuevo-proyecto

El directorio /ejemplos/nuevo-proyecto, es accesible desde el sistema invitado a través del directorio /vagrant.
   
	Directorio anfitrión							Directorio invitado
	/ejemplos/nuevo-proyecto		<===>			/vagrant

La definición de un directorio compartido se refleja en Vagrant a través de la siguiente configuración:

	config.vm.synced_folder [HOST-PATH], [GUEST-PATH]

**Ejercicio**:

    Comprobar la configuración de directorios compartidos en el proyecto *songs-angularjs-app*.

### Trabajando con SSH

El método más habitual de uso de SSH es a través de la línea de comandos, por ejemplo:

	$ ssh bob@example.net

Cuando ejecutamos un sistema invitado, podemos iniciar una sesión SSH de una forma más simple $ vagrant ssh. Este proceso es muy simple porque hace uso de diferentes valores que ya están configurados. Estes valores pueden ser comprobados con el siguiente comando:

	$ vagrant ssh-config

En el caso de que tengamos varias máquinas invitadas corriendo al mismo tiempo, Vagrant se encarga de evitar colisiones en los puertos utilizados para conectarnos con las mismas. Por ejemplo, una segunda máquina invitada tendrá acceso a través del puerto 2200, en lugar del 2222, que ya está ocupado.

De todas formas, siempre se puede utilizar el método clásico de SSH para conectarnos a nuestro sistema invitado:

	$ ssh -p2222 vagrant@127.0.0.1

Las credenciales son las siguientes:

	username: vagrant
	password: vagrant

### Gestionando diferentes sistemas invitados

Cuando trabajamos con sesiones SSH en diferentes máquinas invitadas, podemos llegar a un momento de confusión, y no saber exactamente en que máquina estamos conectados. Para ayudarnos en la tarea de reconocimiento de la máquina invitada, podemos ejecutar el siguiente comando (dentro del invitado):

	$ guestvm

## Riesgo #3

Como ya habremos advertido en las secciones anteriores, las credenciales usadas por defecto en los sistemas invitados, suponen otro riesgo para la seguridad de nuestros sistemas. Sobre todo, porque el usuario 'vagrant' posee permisos de sudo. La única restricción es que se debe iniciar la sesión desde la máquina anfitrión.

Para mitigar este riesgo, debemos modificar la contraseña del usuario 'vagrant':

	$ passwd

### Acceso SSH con llave privada

Aparte del método de acceso SSH a través de las credenciales anteriores, también podemos iniciar una sesión haciendo uso de llaves RSA: una pública y una privada, guardadas en diferentes ficheros.

	~/.ssh/id_rsa     - private key
	~/.ssh/id_rsa.pub - public key

Este es el método utilizado por $ vagrant ssh, con la siguiente configuración:

* La llave privada se guarda en el sistema anfitrión en ``~/.vagrant.d/insecure_private_key``.
* La llave pública se guarda en el sistema invitado en ``/home/vagrant/.ssh/authorized_keys``.

**Ejercicio**:

    Crear un nuevo par de llaves para reemplazar/añadir a las creadas por defecto.

## Riesgo #4

Todas las imágenes que se descargan de Internet incorporan las mismas llaves, por lo que cualquier usuario tiene acceso a ese par de llaves. Las últimas versiones de Vagrant minimizan este riesgo creando un nuevo par durante la creación del sistema invitado ``$ vagrant up``.

### Recargar el sistema invitado

Cuando aplicamos algún cambio en el fichero 'Vagrantfile', para que tengan efecto en el sistema invitado debemos ejecutar el siguiente comando:

	$ vagrant reload

Por ejemplo, podemos modificar el puerto de escucha del proyecto 'Songs for kids', hacia el 9876:

	config.vm.network :forwarded_port, guest: 80, host: 9876, host_ip: "127.0.0.1"

Una vez que recarguemos nuestro sistema, la aplicación estará disponible en http://127.0.0.1:9876.
