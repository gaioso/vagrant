# Creando imágenes desde cero

Hasta el momento, hemos trabajado con ficheros con extensión *.box* que contenían un SO preconfigurado, y que una vez descargados se podían empezar a usar con Vagrant. Pero la pregunta es: ¿Cómo se crean esas imágenes?. Que pasos debemos seguir si queremos usar un SO del cual no encontramos una imagen. En este capítulo veremos como crear una imagen a partir de una imagen ISO, como las que distribuyen en CD/DVD los mantenedores de las distribuciones Linux.

## Packer

Packer es una herramienta de línea de comandos creada por HashiCorp, y cuyo propósito es automatizar la creación de MVs con diferentes sistemas operativos y proveedores. La usaremos para generar imágenes de Vagrant. Lo que hace es descargarse la imagen ISO del SO que queremos instalar. Ejecuta el programa de instalación y se aplica la configuración por defecto. Al finalizar, Packer usa una herramienta de aprovisionamineto (shell, Chef, Puppet o Ansible) para personalizar el sistema. Y finalmente, exporta el sistema para convertirlo en un fichero '.box'.

### Instalación de Packer

Packer está disponible para su descarga en https://www.packer.io/downloads.html. Una vez descargado, se descomprime el fichero, dentro del cual está el binario que debemos hacer accesible dentro de las rutas de la variable PATH. Una vez finalizada la instalación, debemos comprobar su correcto funcionamiento:

	$ packer version

## Creando imágenes con chef/bento

El proyecto más popular con definiciones para Packer es chef/bento, que está disponible en Github: https://github.com/chef/bento.

	# Host
	$ cd directorio/para/ejemplos
	$ git clone https://github.com/chef/bento.git
	$ cd bento

Para proceder con la construcción de la imagen de Ubuntu 14.04 para VirtualBox, ejecutamos la siguiente orden:

	$ packer build --only=virtualbox-iso ubuntu-14.04-i386.json

Una vez finalizada la ejecución, estará disponible el siguiente fichero en nuestro sistema:

	bento/builds/ubuntu-14.04-i386.virtualbox.box

Para poder utilizar la imagen recién creada, debemos añadirla a nuestra lista de 'boxes' en Vagrant.

**Ejercicio**. 
Incorporar esta imagen a nuestra lista de boxes disponibles, y crear una MV a partir de esta nueva imagen.

## Personalizar las imágenes creadas con chef/bento

Para poder crear una versión modificada de cualquiera de las 'boxes' incluídas en el proyecto 'chef/bento', debemos añadir un nuevo script de aprovisionamiento. Vamos a personalizar las imágenes de Ubuntu con el siguiente contenido en el fichero packer/scripts/ubuntu/customize.sh:

	#!/bin/bash -eux

	apt-get install git
	apt-get install mc

A continuación, debemos añadir una referencia a dicho script para que sea ejecutado durante el proceso de generación de la nueva imagen, en el fichero JSON de Ubuntu 14.04 (i386).

De este modo, en la siguiente construcción de una imagen de Ubuntu, llevará incorporado 'git' y 'mc'.

A mayores, también podemos modificar otros parámetros de la imagen a generar. Por ejemplo, podemos modificar las credenciales del usuario creado por defecto. Estos datos son gestionados a través del fichero 'pressed.cfg'.
